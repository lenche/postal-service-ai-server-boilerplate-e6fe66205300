import ast
import os

import firebase_admin
from firebase_admin import credentials

if os.environ.get("ENV") is None:
    raise ValueError("Environment variable ENV not set! Use 'development' or 'testing'")

if os.environ.get("FIREBASE_API_KEY") is None:
    raise ValueError("Environment variable FIREBASE_API_KEY not set!")

if os.environ.get("FIREBASE_SERVICE_ACCOUNT") is None:
    raise ValueError("Environment variable FIREBASE_SERVICE_ACCOUNT not set!")

service_account = ast.literal_eval(os.environ.get("FIREBASE_SERVICE_ACCOUNT"))
firebase_credentials = credentials.Certificate(service_account)


class FirebaseConfig:
    def __init__(self):
        self.firebase = firebase_admin.initialize_app(
            firebase_credentials, {
                "apiKey": os.environ.get("FIREBASE_API_KEY"),
                "authDomain": os.environ.get("FIREBASE_DOMAIN"),
                "databaseURL": os.environ.get("FIREBASE_DATABASE_URL"),
                "storageBucket": os.environ.get("FIREBASE_STORAGE_BUCKET"),
                "serviceAccount": service_account
            })
