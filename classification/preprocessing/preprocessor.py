from classification.preprocessing.transliterator import Transliterator
from classification.preprocessing.transformer import Transformer
from firebase_connection import FirebaseConnection
from models.preprocessing_data import PreprocessingData
from classification.preprocessing import common


class Preprocessor:
    def __init__(self, firebase_connection: FirebaseConnection):
        self.preprocessing_data = firebase_connection.preprocessing_data
        self.transliterator = Transliterator(self.preprocessing_data.alphabet)

        self.stop_words_transformer = Transformer(self.preprocessing_data.stop_words)
        self.stop_sentences_transformer = Transformer(self.preprocessing_data.stop_sentences)
        self.special_chars_transformer = Transformer(self.preprocessing_data.special_chars)
        self.punctuations_transformer = Transformer(self.preprocessing_data.punctuations)

        self.lemms_transformer = Transformer(self.preprocessing_data.lemms)
        self.synonyms_transformer = Transformer(self.preprocessing_data.synonyms)
        self.typos_transformer = Transformer(self.preprocessing_data.typos)

        self.contractions_transformer = Transformer(self.preprocessing_data.contractions)

        if self.preprocessing_data.stop_sentences:
            self.__normalize_stop_sentences()

        if self.preprocessing_data.stop_words:
            self.__normalize_stop_words()

        if self.preprocessing_data.profanities:
            self.__preprocess_profanities()

    def refresh(self, firebase_connection: FirebaseConnection):
        self.__init__(firebase_connection)

    def preprocess_data(self, document: str) -> str:
        document = document.lower()
        document = self.punctuations_transformer.transform(document)
        document = self.special_chars_transformer.transform(document)
        document = self.transliterate(document)
        document = self.__normalization(document)
        normalized_document = document

        document = self.stop_sentences_transformer.transform(document)
        document = self.__stemming(document, normalized_document)
        document = self.synonyms_transformer.transform(document)
        document = self.stop_words_transformer.transform(document)
        document = self.typos_transformer.transform(document)
        document = self.lemms_transformer.transform(document)

        document = common.remove_consecutive_characters(document)

        if len(document) > 0:
            return document

        return normalized_document

    def transliterate(self, document):
        return self.transliterator.transliterate(document)

    def __normalization(self, document: str):
        document = document.lower()
        document = self.contractions_transformer.transform(document)
        document = common.remove_consecutive_characters(document)
        document = common.remove_consecutive_words(document)

        return document

    def __normalize_stop_words(self):
        normalized = []
        for stop_sentence in self.preprocessing_data.stop_words["items"]:
            stop_sentence = self.__normalization(stop_sentence)
            normalized.append(stop_sentence)

        self.preprocessing_data.stop_words["items"] = normalized

    def __normalize_stop_sentences(self):
        normalized = []
        for stop_sentence in self.preprocessing_data.stop_sentences["items"]:
            stop_sentence = self.__normalization(stop_sentence)
            normalized.append(stop_sentence)

        self.preprocessing_data.stop_sentences["items"] = normalized

    def __preprocess_profanities(self):
        self.preprocessing_data.profanities = [p.lower() for p in self.preprocessing_data.profanities]
        self.preprocessing_data.profanities = \
            [self.contractions_transformer.transform(p) for p in self.preprocessing_data.profanities]

    def __stemming(self, document, normalized_document):
        words = document.split(" ")
        new_words = [self.__remove_prefixes(word) for word in words]
        new_words = [self.__remove_suffixes(word) for word in new_words]

        stemmed = " ".join(new_words)
        stemmed = stemmed.strip()
        if len(stemmed) == 0:
            return normalized_document

        return stemmed

    def __remove_suffixes(self, word):
        if not self.preprocessing_data.suffixes:
            return word

        if self.__word_in_safe_words(word):
            return word

        for suffix in self.preprocessing_data.suffixes["items"]:
            if word.endswith(suffix):
                result = word[:-len(suffix)]
                if len(result) > 2:
                    return result
        return word

    def __remove_prefixes(self, word):
        if not self.preprocessing_data.prefixes:
            return word

        if self.__word_in_safe_words(word):
            return word
        for prefix in self.preprocessing_data.prefixes["items"]:
            if word.startswith(prefix):
                result = word[len(prefix):]
                if len(result) > 2:
                    return result
        return word

    def __word_in_safe_words(self, word: str) -> bool:
        for key in self.preprocessing_data.safe_words["items"]:
            safe_words_list = self.preprocessing_data.safe_words["items"][key]
            if word in safe_words_list:
                return True

        return False
