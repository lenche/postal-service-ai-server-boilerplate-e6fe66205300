from itertools import groupby


def remove_consecutive_characters(word: str, max_characters=1) -> str:
    result = ''

    for k, g in groupby(word):
        if k.isalpha() or k == ' ':
            for c in list(g)[:max_characters]:
                result = result + c
        else:
            for c in list(g):
                result = result + c

    return result


class ProfanityChecker:
    def __init__(self, profanities):
        self.profanities = profanities

    def contains_profanity(self, preprocessed_question: str):
        if not self.profanities:
            return False

        response = Response(False, [])

        for profanity in self.profanities:
            preprocessed_question = " " + preprocessed_question + " "
            if profanity in preprocessed_question:
                response.has_profanity = True
                response.found_profanities.append(profanity)
                # break

        return response


class Response:
    def __init__(self, has_profanity, found_profanities):
        self.has_profanity = has_profanity
        self.found_profanities = found_profanities
