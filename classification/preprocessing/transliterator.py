class Transliterator:
    def __init__(self, alphabet):
        self.alphabet = alphabet

    def transliterate(self, document: str) -> str:
        if not self.alphabet:
            return document.lower()

        document = document.lower()
        new_document = ""
        for letter in document:
            if letter.isdigit() | letter.isspace():
                new_document += letter
            else:
                if letter in self.alphabet:
                    transliterated = self.alphabet[letter]
                    if transliterated:
                        letter = transliterated
                new_document += letter

        if len(new_document) == 0:
            new_document = document

        return new_document
