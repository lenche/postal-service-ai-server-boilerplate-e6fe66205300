import re
from classification.preprocessing import common


class Transformer:
    def __init__(self, transformations: dict):
        self.empty_data = False

        if not transformations:
            self.empty_data = True
            return

        self.type = transformations["type"]
        self.subtype = transformations["subtype"]
        self.is_subword = transformations["is_subword"]
        self.items = transformations["items"]

        if self.type == "regular":
            self.__normalize_items()
            return
        if self.type != "regex":
            return

        if self.subtype == "remove" and \
                self.is_subword:
            self.__compile_regexes_remove_subword()
        elif self.subtype == "remove":
            self.__compile_regexes_remove()
        else:
            self.__compile_regexes_replace()

    def transform(self, document):
        if self.empty_data:
            return document

        if self.type == "regex":
            if self.subtype == "remove":
                return self.__transform_remove_regex(document)
            elif self.subtype == "replace":
                return self.__transform_regex(document)

        elif self.type == "regular":
            if self.subtype == "remove":
                return self.__transform_remove(document)
            elif self.subtype == "replace":
                return self.__transform_regex(document)

    def __normalize_items(self):
        for i in range(len(self.items)):
            item = self.items[i]
            item = common.remove_consecutive_characters(item)
            self.items[i] = item

    def __compile_regexes_replace(self):
        items = dict()
        for key in self.items:
            regexes = self.items[key]
            key = common.remove_consecutive_characters(key)
            for regex in regexes:
                regex = common.remove_consecutive_characters(regex)
                compiled = re.compile("\\b" + regex + "\\b", re.I | re.U)
                items[compiled] = key

        self.items = items

    def __compile_regexes_remove_subword(self):
        items = []
        for regex in self.items:
            regex = common.remove_consecutive_characters(regex)
            compiled = re.compile(regex, re.I | re.U)
            items.append(compiled)

        self.items = items

    def __compile_regexes_remove(self):
        items = []
        for regex in self.items:
            regex = common.remove_consecutive_characters(regex)
            compiled = re.compile("\\b" + regex + "\\b", re.I | re.U)
            items.append(compiled)

        self.items = items

    def __transform_remove_regex(self, document):
        for pattern in self.items:
            document = pattern.sub("", document)

        return document.strip()

    def __transform_remove(self, document):
        for item in self.items:
            document = document.replace(item, "")
        return document.strip()

    @staticmethod
    def __transform_regular(document):
        return document

    def __transform_regex(self, document):
        document = document.replace("'", "’")

        for pattern, rep in self.items.items():
            document = pattern.sub(rep, document)

        return document.lower()
