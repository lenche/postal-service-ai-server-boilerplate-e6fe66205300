import re
from itertools import groupby


def remove_consecutive_characters(word: str, max_characters=1) -> str:
    result = ""

    for k, g in groupby(word):
        if k.isalpha() or k == " ":
            for c in list(g)[:max_characters]:
                result = result + c
        else:
            for c in list(g):
                result = result + c
    return result


def remove_consecutive_words(sentence: str) -> str:
    if type(sentence) == list:
        return ""

    no_punc = re.sub(r"[^\w\s\\’\"]", "", sentence, re.UNICODE)
    re_output = re.sub(r"\b(\w+)( \1\b)+", r"\1", no_punc)

    return re_output
