import time
from multiprocessing.shared_memory import SharedMemory

from typing import List

from gensim.corpora import Dictionary
from gensim.similarities import MatrixSimilarity

from classification.preprocessing.preprocessor import Preprocessor
from models.classifiers_data import ClassifiersData
import gensim

from firebase_connection import FirebaseConnection
from models.repositories.classifiers import ClassifiersRepo
from models.repositories.dictionaries import DictionaryRepo
from models.train_data import TrainData

from logger import Logger

log = Logger()


def get_current_time_as_bytes() -> bytes:
    from datetime import datetime
    now = datetime.now()
    time_str = now.strftime("%H:%M:%S")
    return str.encode(time_str)


class Trainer:
    def __init__(self, classifiers_repo: ClassifiersRepo, dictionary_repo: DictionaryRepo,
                 firebase: FirebaseConnection, classifiers_container: ClassifiersData, preprocessor: Preprocessor,
                 lang: str,
                 train_data_version: SharedMemory):
        self.classifiers_repo = classifiers_repo
        self.dictionary_repo = dictionary_repo
        self.firebase = firebase
        self.__classifiers = classifiers_container
        self.preprocessor = preprocessor
        self.lang = lang
        self.train_data_version = train_data_version
        self.train_data_version.buf[:8] = get_current_time_as_bytes()

    def refresh(self):
        all_classifiers_dict = self.classifiers_repo.get_all_classifiers()
        dictionary = self.dictionary_repo.get_dictionary()
        self.__classifiers.set_dictionary(dictionary)
        self.__classifiers.set_train_data(all_classifiers_dict)

    def start_training(self) -> None:
        self.firebase.refresh()
        self.preprocessor.refresh(self.firebase)
        log.info("started training the [{}] classifiers".format(self.lang))

        start_time = time.time()

        if not self.firebase.faqs_data:
            log.error("the training has stopped because there are no [{}] faq in the db".format(self.lang))
            log.info_end_time(start_time, "stopped training")

            return

        faq_train_data = self.__create_train_data_for_faq(self.firebase.faqs_data)
        combined_temp_dictionaries = list(faq_train_data.temp_dict)

        scenarios = {}

        if self.firebase.states_data:
            for tmp_state_id in self.firebase.states_data:
                state_train_data = self.__create_train_data_for_state(tmp_state_id, self.firebase.states_data,
                                                                      self.firebase.choices_data,
                                                                      self.firebase.parent_choices_data)
                scenarios[tmp_state_id] = state_train_data
                combined_temp_dictionaries.extend(state_train_data.temp_dict)

            for key in scenarios:
                stuff = scenarios.get(key)
                stuff.temp_dict = None

        dictionary = gensim.corpora.Dictionary(combined_temp_dictionaries)

        self.__classifiers.set_dictionary(dictionary)
        self.__prepare_train_data_for_container(dictionary, faq_train_data)

        train_data_by_id = dict()
        train_data_by_id["faq"] = faq_train_data

        if self.firebase.states_data:
            for state_id in self.firebase.states_data:
                self.__prepare_train_data_for_container(dictionary, scenarios[state_id])
                train_data_by_id[state_id] = scenarios[state_id]

        self.__classifiers.set_train_data(train_data_by_id)

        self.__insert_data_in_db(dictionary, train_data_by_id)

        log.info_end_time(start_time, "finished training the [{}] data".format(self.lang))
        self.train_data_version.buf[:8] = get_current_time_as_bytes()

    def get_classifiers(self):
        return self.__classifiers

    def create_train_data_for_descriptions(self, documents):
        preprocessed_documents = []

        for document in documents:
            doc = self.preprocessor.preprocess_data(document)
            preprocessed_documents.append(doc)

        return preprocessed_documents

    def create_train_data_for_custom_state(self, buttons):
        state_docs = []
        state_class_ids = []

        for button in buttons:
            for document in button["descriptions"]:
                doc = self.preprocessor.transliterate(document)
                state_docs.append(doc)
                state_class_ids.append(button["id"])

        temp_dictionary = self.__generate_dictionary(state_docs)

        train_data = TrainData(state_docs, state_class_ids, temp_dictionary)

        dictionary = gensim.corpora.Dictionary(list(train_data.temp_dict))

        self.__prepare_train_data_for_container(dictionary, train_data)

        result = dict()
        result["train_data"] = train_data
        result["dictionary"] = dictionary
        return result

    def __insert_data_in_db(self, dictionary,
                            train_data_by_id):
        log.info("inserting trained data in the [{}] db, might take a while".format(self.lang))

        self.dictionary_repo.save_dictionary(dictionary)
        self.classifiers_repo.delete_all_classifiers()
        self.classifiers_repo.insert_or_update(train_data_by_id)

    @staticmethod
    def __tokenize(string):
        return string.split(" ")

    def __generate_dictionary(self, docs: List[str]) -> List[List[str]]:
        return [[w.lower() for w in self.__tokenize(text)] for text in docs]

    def __create_train_data_for_faq(self, all_faqs_data) -> TrainData:
        log.info("creating train data for the [{}] FAQ".format(self.lang))

        start_time = time.time()

        train_docs = []
        faq_class_ids = []

        for key in all_faqs_data:
            faq_data = all_faqs_data.get(key)
            current_faq_docs = []
            documents = faq_data["documents"]
            for document in documents:
                preprocessed_doc = self.preprocessor.preprocess_data(document)
                train_docs.append(preprocessed_doc)
                if preprocessed_doc not in current_faq_docs:
                    current_faq_docs.append(preprocessed_doc)

                faq_class_ids.append(key)

        temp_dict = self.__generate_dictionary(train_docs)

        log.info_end_time(start_time, "finished creating train data for the [{}] FAQ".format(self.lang))

        return TrainData(train_docs, faq_class_ids, temp_dict)

    def __create_train_data_for_state(self, state_id: str, states_classifiers, choices_train_docs,
                                      choices_parent_data) -> TrainData:
        log.info("creating train data for the [{}] state with id [{}]".format(self.lang, state_id))

        start_time = time.time()

        train_docs = []
        state_class_ids = []

        state_classifier_data = states_classifiers.get(state_id)
        for choice_id in state_classifier_data:
            choice_data = state_classifier_data[choice_id]

            if "train_data_id" in choice_data:
                train_data_id = choice_data["train_data_id"]
                choice_train_data = choices_train_docs[train_data_id]

                for document in choice_train_data["documents"]:
                    preprocessed_doc = self.preprocessor.preprocess_data(document)
                    train_docs.append(preprocessed_doc)
                    state_class_ids.append(choice_id)

            if "parent_train_data_id" in choice_data:
                parent_train_data_id = choice_data["parent_train_data_id"]
                if parent_train_data_id not in choices_parent_data:
                    log.error("missing parent train docs [{}] for the choice [{}] in the state [{}]"
                              .format(parent_train_data_id, choice_id, state_id))
                else:
                    parent_train_data = choices_parent_data[parent_train_data_id]
                    for document in parent_train_data["documents"]:
                        preprocessed_doc = self.preprocessor.preprocess_data(document)
                        train_docs.append(preprocessed_doc)
                        state_class_ids.append(choice_id)

        temp_dictionary = self.__generate_dictionary(train_docs)

        log.info_end_time(start_time,
                          "finished creating train data for the [{}] state with id [{}]".format(self.lang, state_id))

        return TrainData(train_docs, state_class_ids, temp_dictionary)

    def __prepare_train_data_for_container(self, dictionary: Dictionary, train_data: TrainData):
        gen_docs = [[w.lower() for w in self.__tokenize(text)] for text in train_data.train_docs]

        corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]

        tf_idf = gensim.models.TfidfModel(corpus)
        train_data.set_tf_idf(tf_idf)

        sims = MatrixSimilarity(tf_idf[corpus], num_features=len(dictionary))
        train_data.set_sims(sims)
