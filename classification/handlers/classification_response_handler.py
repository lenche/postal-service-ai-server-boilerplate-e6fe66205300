import os
from typing import List

from gensim.corpora import Dictionary

from models.classifiers_data import ClassifiersData
from models.train_data import TrainData
from logger import Logger

log = Logger()

FAQ_THRESHOLD = float(os.environ.get("FAQ_THRESHOLD"))
SCENARIO_THRESHOLD = float(os.environ.get("SCENARIO_THRESHOLD"))
DECREASED_SCENARIO_THRESHOLD = float(os.environ.get("DECREASED_SCENARIO_THRESHOLD"))
CUSTOM_TRAINING_STATE_THRESHOLD = float(os.environ.get("CUSTOM_TRAINING_STATE_THRESHOLD"))

response_type_missclassification = "missclassification"
response_type_profanity = "profanity"
response_type_open_ended_response = "open_ended_question"
response_type_scenario = "scenario"
response_type_faq = "faq"


def round_similarity(x):
    percent = round(float(x) * 100)
    if percent > 100:
        percent = 100
    return percent / 100


class ClassificationResponseHandler:
    def __init__(self, lang):
        self.lang = lang

    def missclassification_response(self, question):
        response_data = self.__generate_default_response_params(question, question)
        return self.__generate_missclassification_response_data(response_data)

    def scenario_response(self, preprocessed_question: str, question: str, state_id: str,
                          classifiers_container: ClassifiersData):
        response_data = self.__generate_default_response_params(question, preprocessed_question)
        response_data["type"] = response_type_scenario

        state_classifier_data = classifiers_container.get_classifier_data(state_id)
        state_similarity_results = self.__find_similarity(state_classifier_data, preprocessed_question,
                                                          classifiers_container.dictionary)

        faq_classifier_data = classifiers_container.get_faq_classifier_data()
        faq_similarity_results = self.__find_similarity(faq_classifier_data, preprocessed_question,
                                                        classifiers_container.dictionary)

        state_best_similarity = self.__get_best_similarity(state_similarity_results)
        state_best_position = self.__get_best_position(state_similarity_results)

        faq_best_similarity = self.__get_best_similarity(faq_similarity_results)
        faq_best_position = self.__get_best_position(faq_similarity_results)

        if state_best_similarity == faq_best_similarity and \
                state_best_similarity == 0:
            return self.__generate_missclassification_response_data(response_data, state_similarity_results,
                                                                    state_classifier_data)

        if faq_best_similarity > FAQ_THRESHOLD:
            faq_better_match_similarity = self.__check_for_faq_better_match(preprocessed_question,
                                                                            state_best_similarity,
                                                                            state_classifier_data.train_docs[
                                                                                state_best_position],
                                                                            faq_best_similarity,
                                                                            faq_classifier_data.train_docs[
                                                                                faq_best_position])
            if faq_better_match_similarity:
                faq_classifier_data = classifiers_container.get_faq_classifier_data()
                return self.__generate_faq_response_data(response_data, faq_similarity_results, faq_classifier_data)

        return self.__generate_scenario_response_data(response_data, state_similarity_results, state_classifier_data)

    def prefer_scenarios_in_classification_response(self, preprocessed_question: str, question: str,
                                                    state_id: str,
                                                    classifiers_container: ClassifiersData):
        response_data = self.__generate_default_response_params(question, preprocessed_question)
        response_data["type"] = response_type_scenario

        classifier_data = classifiers_container.get_classifier_data(state_id)
        similarity_results = self.__find_similarity(classifier_data, preprocessed_question,
                                                    classifiers_container.dictionary)

        return self.__generate_decrease_scenario_response(response_data, similarity_results, classifier_data)

    def faq_response(self, preprocessed_question: str, question: str, classifiers_container: ClassifiersData):
        response_data = self.__generate_default_response_params(question, preprocessed_question)

        classifier_data = classifiers_container.get_faq_classifier_data()
        similarity_results = self.__find_similarity(classifier_data, preprocessed_question,
                                                    classifiers_container.dictionary)

        return self.__generate_faq_response_data(response_data, similarity_results, classifier_data)

    def train_classifier_with_custom_data_response(self, preprocessed_question: str, question: str,
                                                   train_data: TrainData,
                                                   dictionary: Dictionary):
        response_data = self.__generate_default_response_params(question, preprocessed_question)

        similarity_results = self.__find_similarity(train_data, preprocessed_question, dictionary)
        best_similarity = self.__get_best_similarity(similarity_results)

        if best_similarity < CUSTOM_TRAINING_STATE_THRESHOLD:
            response_data["type"] = response_type_missclassification
            return response_data

        return self.__generate_scenario_response_data(response_data, similarity_results, train_data)

    def open_ended_question_response(self, preprocessed_question: str, question: str):
        response_data = self.__generate_default_response_params(question, preprocessed_question)
        response_data["type"] = response_type_open_ended_response

        log.debug("sending a open ended question response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def profanity_response(self, question: str, preprocessed_question: str, profanities: []):
        response_data = self.__generate_default_response_params(question, preprocessed_question)
        response_data["type"] = response_type_profanity
        response_data["profanities"] = profanities

        log.debug("sending a profanity response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def required_params_error_response(self):
        response_data = {"error": "please check the parameters"}

        log.debug("sending a required params response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def __generate_default_response_params(self, question: str, preprocessed_question: str) -> dict:
        return {"lang": self.lang, "input": {"question": question, "preprocessed_question": preprocessed_question}}

    def __generate_scenario_response_data(self, response_data: dict, similarity_results: List, train_data: TrainData):
        response_data["type"] = response_type_scenario
        results = []

        best_similarity = self.__get_best_similarity(similarity_results)
        if best_similarity < SCENARIO_THRESHOLD:
            return self.__generate_missclassification_response_data(response_data, similarity_results, train_data)

        for similarity_result in similarity_results:
            similarity = similarity_result["sim"]
            position = similarity_result["pos"]
            if similarity > 0:
                results.append({
                    "similarity": float(similarity),
                    "most_similar": train_data.train_docs[position],
                    "class_id": train_data.class_ids[position]
                })

        response_data["results"] = results

        log.debug("sending a scenario response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def __generate_faq_response_data(self, response_data, similarity_results, train_data):
        response_data["type"] = response_type_faq
        results = []

        best_similarity = self.__get_best_similarity(similarity_results)
        if best_similarity < FAQ_THRESHOLD:
            return self.__generate_missclassification_response_data(response_data, similarity_results, train_data)

        for res in similarity_results:
            sim = res["sim"]
            pos = res["pos"]
            if sim > 0:
                results.append({
                    "similarity": float(sim),
                    "class_id": train_data.class_ids[pos],
                    "most_similar": train_data.train_docs[pos]
                })

        response_data["results"] = results

        log.debug("sending a faq response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def __generate_missclassification_response_data(self, response_data, similarity_result=None,
                                                    train_data=None) -> dict:
        response_data["type"] = response_type_missclassification

        if not similarity_result and not train_data:
            return response_data

        results = []

        for res in similarity_result:
            sim = res["sim"]
            pos = res["pos"]
            if sim > 0:
                result = {
                    "similarity": float(sim),
                    "most_similar": train_data.train_docs[pos],
                    "class_id": train_data.class_ids[pos]
                }
                results.append(result)

        response_data["results"] = results

        log.debug("sending a missclassification response, lang {}, data {}".format(self.lang, response_data))
        return response_data

    def __generate_decrease_scenario_response(self, response_data: dict, similarity_results: List,
                                              train_data: TrainData):
        best_similarity = self.__get_best_similarity(similarity_results)
        if best_similarity < DECREASED_SCENARIO_THRESHOLD:
            return self.__generate_missclassification_response_data(response_data, similarity_results, train_data)

        return self.__generate_scenario_response_data(response_data, similarity_results, train_data)

    def __find_similarity(self, train_data: TrainData, question: str, dictionary: Dictionary, max_elements=5) -> List:
        query_doc = [w.lower() for w in self.__tokenize(question)]

        query_doc_bow = dictionary.doc2bow(query_doc)
        query_doc_tf_idf = train_data.tf_idf[query_doc_bow]
        similarities = train_data.sims[query_doc_tf_idf]

        result = [{"sim": round_similarity(similarities[i]), "pos": i} for i in range(len(similarities))]
        result.sort(key=lambda x: x['sim'], reverse=True)

        return result[:max_elements]

    def __check_for_faq_better_match(self, preprocessed_question, state_similarity, state_most_similar_doc,
                                     faq_similarity, faq_most_similar_doc):

        state_length_mismatch = self.__get_length_mismatch(preprocessed_question, state_most_similar_doc)
        faq_length_mismatch = self.__get_length_mismatch(preprocessed_question, faq_most_similar_doc)

        faq_similarity = faq_similarity * faq_length_mismatch
        state_similarity = state_similarity * state_length_mismatch

        if faq_similarity > state_similarity:
            return faq_similarity

    @staticmethod
    def __get_length_mismatch(preprocessed_question: str, most_similar_doc: str) -> float:
        length_user_sentence = len(preprocessed_question.split(" "))
        length_doc = len(most_similar_doc.split(" "))

        mismatch = length_user_sentence / length_doc
        if mismatch > 1:
            mismatch = length_doc / length_user_sentence

        return mismatch

    @staticmethod
    def __get_best_similarity(similarity_results):
        best_similarity_result = similarity_results[0]
        best_similarity = best_similarity_result["sim"]
        return best_similarity

    @staticmethod
    def __get_best_position(similarity_results):
        best_similarity_result = similarity_results[0]
        best_similarity = best_similarity_result["pos"]
        return best_similarity

    @staticmethod
    def __tokenize(string):
        return string.split(' ')
