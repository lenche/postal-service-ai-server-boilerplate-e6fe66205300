from api.request_handler import RequestHandler
from classification.trainer import Trainer


class TrainRequestHandler(RequestHandler):
    def __init__(
            self,
            trainer: Trainer,
            lang: str
    ):
        self.trainer = trainer
        self.lang = lang

    def endpoint(self) -> str:
        return "/train/{}".format(self.lang)

    def handle_request(self, request_data):
        self.trainer.start_training()
        return {"status": "Retrained"}
