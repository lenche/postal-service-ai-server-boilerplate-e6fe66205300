from api.request_handler import RequestHandler
from classification.trainer import Trainer


class PreviewPreprocessedDataRequestHandler(RequestHandler):
    def __init__(
            self,
            trainer: Trainer,
            lang: str
    ):
        self.trainer = trainer
        self.lang = lang

    def endpoint(self) -> str:
        return "/preview/{}".format(self.lang)

    def handle_request(self, request_data):
        if "documents" in request_data:
            documents = request_data["documents"]
            result = self.trainer.create_train_data_for_descriptions(documents)
            return {"documents": result}
        return {"error": "'documents' is a required field."}
