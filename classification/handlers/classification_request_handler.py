import os
import array
from multiprocessing.shared_memory import SharedMemory

from api.request_handler import RequestHandler
from classification.handlers.classification_response_handler import ClassificationResponseHandler
from classification.preprocessing.preprocessor import Preprocessor
from classification.preprocessing.profanity_checker import ProfanityChecker
from classification.trainer import Trainer
from firebase_connection import FirebaseConnection

from logger import Logger

log = Logger()

prefer_scenarios_in_classification = "prefer_scenarios_in_classification"
custom_training_state = "custom_training_state"
open_ended_question = "open_ended_question"

request_type_faq = "faq"
request_type_scenario = "scenario"
request_type_first_message = "first_message"

response_type_missclassification = "missclassification"


class ClassificationRequestHandler(RequestHandler):
    def __init__(
            self,
            classification_response_handler: ClassificationResponseHandler,
            firebase_connection: FirebaseConnection,
            trainer: Trainer,
            profanity_checker: ProfanityChecker,
            preprocessor: Preprocessor,
            lang: str,
            data_changed_shared_memory: SharedMemory):
        self.classification_response_handler = classification_response_handler
        self.profanity_checker = profanity_checker

        self.firebase_connection = firebase_connection
        self.preprocessing_data = firebase_connection.preprocessing_data
        self.trainer = trainer
        self.preprocessor = preprocessor
        self.lang = lang
        self.data_changed_sm = data_changed_shared_memory
        self.train_data_version = array.array('b', self.data_changed_sm.buf[:8])

    def endpoint(self) -> str:
        return "/classify/{}".format(self.lang)

    def handle_request(self, request_data):
        last_train_data_version = array.array('b', self.data_changed_sm.buf[:8])
        if self.train_data_version != last_train_data_version:
            self.trainer.refresh()
            self.firebase_connection.refresh()
            self.preprocessor.refresh(self.firebase_connection)
            self.train_data_version = last_train_data_version

        if not self.__required_params(request_data):
            return self.classification_response_handler.required_params_error_response()

        if self.trainer.get_classifiers().is_empty():
            return self.classification_response_handler.missclassification_response(request_data["question"])

        question = request_data["question"]
        request_data["preprocessed_question"] = self.preprocessor.preprocess_data(question)
        preprocessed_question = request_data["preprocessed_question"]

        profanity_check = self.profanity_checker.contains_profanity(question)
        if profanity_check.has_profanity:
            return self.__handle_profanity_request(question, preprocessed_question, profanity_check.found_profanities)

        request_type = request_data["type"]

        if request_type == request_type_faq:
            return self.__handle_faq_request(preprocessed_question, question)

        state_data = request_data["state_data"]
        state_id = state_data["id"]

        if self.__should_train_classifier_with_custom_data(request_data):
            choices = state_data["choices"]
            return self.__handle_train_classifier_with_custom_data(question, preprocessed_question, choices)

        if not self.trainer.get_classifiers().classifier_exists(state_id):
            conversation_topics = None
            if "conversation_topics" in request_data:
                conversation_topics = request_data["conversation_topics"]
            preprocessed_question = self.__add_conversation_topic(preprocessed_question, conversation_topics)
            return self.__handle_faq_request(preprocessed_question, question)

        if self.__is_open_ended_question(request_data):
            return self.__handle_open_ended_question_request(preprocessed_question, question, state_id)

        if self.__should_prefer_scenarios_in_classification(request_data):
            return self.__handle_prefer_scenarios_in_classification(preprocessed_question, question, state_id)

        return self.__handle_scenario_request(preprocessed_question, question, state_id)

    def __handle_prefer_scenarios_in_classification(self, preprocessed_question: str, question: str, state_id: str):
        return self.classification_response_handler. \
            prefer_scenarios_in_classification_response(preprocessed_question, question, state_id,
                                                        self.trainer.get_classifiers())

    def __handle_scenario_request(self, preprocessed_question: str, question: str, state_id: str):
        log.debug("handling scenario request, lang: [{}]...".format(self.lang))

        return self.classification_response_handler. \
            scenario_response(preprocessed_question, question, state_id, self.trainer.get_classifiers())

    def __handle_faq_request(self, preprocessed_question: str, question: str):
        log.debug("handling faq request, lang: [{}]...".format(self.lang))

        return self.classification_response_handler. \
            faq_response(preprocessed_question, question, self.trainer.get_classifiers())

    def __handle_train_classifier_with_custom_data(self, question: str, preprocessed_question: str, buttons):
        log.debug("handling custom training state request, lang: [{}]...".format(self.lang))

        result = self.trainer.create_train_data_for_custom_state(buttons)
        train_data = result["train_data"]
        dictionary = result["dictionary"]

        return self.classification_response_handler. \
            train_classifier_with_custom_data_response(preprocessed_question, question, train_data, dictionary)

    def __handle_open_ended_question_request(self, preprocessed_question: str, question: str, state_id: str):
        log.debug("handling open ended question request, lang: [{}]...".format(self.lang))

        result = self.__handle_scenario_request(preprocessed_question, question, state_id)

        if result["type"] == response_type_missclassification:
            return self.classification_response_handler. \
                open_ended_question_response(preprocessed_question, question)

        return result

    def __handle_profanity_request(self, question, preprocessed_question, profanities):
        return self.classification_response_handler. \
            profanity_response(question, preprocessed_question, profanities)

    @staticmethod
    def __add_conversation_topic(preprocessed_question, conversation_topics):
        if conversation_topics is not None and \
                os.environ.get("SHOULD_USE_CONVERSATION_TOPICS") == "true":
            for topic in conversation_topics:
                if len(topic) > 0:
                    preprocessed_question = preprocessed_question + " " + topic

        return preprocessed_question.strip()

    @staticmethod
    def __should_prefer_scenarios_in_classification(request_data: dict) -> bool:
        if "classify_type" not in request_data:
            return False
        return request_data["classify_type"] == prefer_scenarios_in_classification

    @staticmethod
    def __should_train_classifier_with_custom_data(request_data: dict) -> bool:
        if "classify_type" not in request_data:
            return False
        return request_data["classify_type"] == custom_training_state

    @staticmethod
    def __is_open_ended_question(request_data: dict) -> bool:
        if "classify_type" not in request_data:
            return False
        return request_data["classify_type"] == open_ended_question

    @staticmethod
    def __required_params(request_data: dict) -> bool:
        if "type" not in request_data:
            return False

        req_type = request_data["type"]

        if "question" not in request_data:
            return False

        if req_type == request_type_scenario and \
                "state_data" not in request_data:
            return False

        return True
