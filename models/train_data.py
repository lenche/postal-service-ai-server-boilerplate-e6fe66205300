import pickle


class TrainData:
    def __init__(self, docs, class_ids, temp_dict):
        self.train_docs = docs
        self.class_ids = class_ids
        self.temp_dict = temp_dict
        self.sims = None
        self.tf_idf = None

    def set_sims(self, sims):
        self.sims = sims

    def set_tf_idf(self, tf_idf):
        self.tf_idf = tf_idf

    @staticmethod
    def from_pickle(file_loc: str):
        with open(file_loc, "rb") as fp:
            data = pickle.load(fp)

        return data

    @staticmethod
    def from_pickle_string(pickle_string: str):
        data = pickle.loads(pickle_string)
        return data
