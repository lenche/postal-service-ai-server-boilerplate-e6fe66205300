import pickle

from gensim.corpora import Dictionary
from sqlalchemy.engine import Connection


class DictionaryRepo:
    def __init__(self, conn: Connection, lang: str):
        self.DICTIONARY_NAME = "{}_dictionary_data".format(lang)
        self.DICTIONARY_COLUMN = "data"

        self.conn = conn
        command = "create table if not exists {}({} bytea not null)".format(self.DICTIONARY_NAME,
                                                                            self.DICTIONARY_COLUMN)
        result = conn.execute(command)
        result.close()

    def save_dictionary(self, dictionary: Dictionary):
        pickled_dict = pickle.dumps(dictionary)
        command = "delete from {}".format(self.DICTIONARY_NAME)
        result = self.conn.execute(command)
        result.close()
        command = "insert into {} values(%s)".format(self.DICTIONARY_NAME)
        result = self.conn.execute(command, pickled_dict)
        result.close()

    def get_dictionary(self):
        command = "select * from {}".format(self.DICTIONARY_NAME)
        result = self.conn.execute(command)
        pickled_dict = result.next()[self.DICTIONARY_COLUMN]
        return pickle.loads(pickled_dict)
