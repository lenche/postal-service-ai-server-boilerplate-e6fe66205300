import pickle
from typing import List, Dict

from sqlalchemy.engine import Connection

from models.train_data import TrainData


class ClassifiersRepo:
    def __init__(self, conn: Connection, lang: str):
        self.TABLE_NAME = "{}_corpus_data".format(lang)
        self.STATE_ID_COLUMN = "state_id"
        self.DATA_COLUMN = "data"

        command = "create table if not exists {} ({} text primary key, {} bytea not null)" \
            .format(self.TABLE_NAME, self.STATE_ID_COLUMN, self.DATA_COLUMN)
        result = conn.execute(command)
        result.close()
        self.conn = conn

    def get_specific_classifier(self, state_id: str) -> TrainData:
        command = "select * from {} where {}=%s".format(self.TABLE_NAME, self.STATE_ID_COLUMN)
        result = self.conn.execute(command, state_id)
        state_data = result.next()[self.DATA_COLUMN]
        result.close()

        return TrainData.from_pickle_string(state_data)

    def get_specific_classifiers(self, state_ids: List[str]) -> Dict[str, TrainData]:
        deserialized = [self.get_specific_classifier(state_id) for state_id in state_ids]
        return dict(zip(state_ids, deserialized))

    def get_all_classifiers(self) -> Dict[str, TrainData]:
        command = "select * from {}".format(self.TABLE_NAME)
        result = self.conn.execute(command)
        to_return = {}
        for row in result:
            state_id = row[self.STATE_ID_COLUMN]
            state_data = row[self.DATA_COLUMN]
            stuff = TrainData.from_pickle_string(state_data)
            to_return[state_id] = stuff
        result.close()
        return to_return

    def has_any_classifier(self) -> bool:
        command = "select 1 from {}".format(self.TABLE_NAME)
        result = self.conn.execute(command)
        result.close()
        return result.rowcount > 0

    def delete_all_classifiers(self):
        command = "delete from {}".format(self.TABLE_NAME)
        self.conn.execute(command)

    def insert_or_update(self, states: Dict[str, TrainData]):
        for state_id in states:
            state = states[state_id]
            pickled_state = pickle.dumps(state)

            command = "select {} from {} where state_id=%s".format(self.STATE_ID_COLUMN, self.TABLE_NAME)
            exists_result = self.conn.execute(command, state_id)
            exists_result.close()

            if exists_result.rowcount > 0:
                command = "update {} set data=%s where state_id=%s".format(self.TABLE_NAME)
                result = self.conn.execute(command, pickled_state, state_id)

            else:
                command = "insert into {} values(%s, %s)".format(self.TABLE_NAME)
                result = self.conn.execute(command, state_id, pickled_state)
            result.close()
