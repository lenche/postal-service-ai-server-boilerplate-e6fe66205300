from typing import Dict
from gensim.corpora import Dictionary
from models.train_data import TrainData


class ClassifiersData:
    def __init__(self, deserialized_train_data_by_id: Dict[str, TrainData], dictionary: Dictionary = None):
        self.train_data_by_id = dict(deserialized_train_data_by_id)
        self.dictionary = dictionary

    def classifier_exists(self, classifier_id: str) -> bool:
        if classifier_id:
            return classifier_id in self.train_data_by_id

    def get_classifier_data(self, classifier_id: str) -> TrainData:
        return self.train_data_by_id[classifier_id]

    def set_train_data(self, new_train_data: Dict[str, TrainData]):
        self.train_data_by_id = dict(new_train_data)

    def set_dictionary(self, new_dict: Dictionary) -> None:
        self.dictionary = new_dict

    def get_faq_classifier_data(self) -> TrainData:
        return self.get_classifier_data("faq")

    def is_empty(self):
        return len(self.train_data_by_id) == 0
