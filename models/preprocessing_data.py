class PreprocessingData:
    def __init__(self, preprocessing_obj):
        self.alphabet = preprocessing_obj.get("alphabet", None)
        self.profanities = preprocessing_obj.get("profanities", None)

        stemming = preprocessing_obj.get("stemming", dict())
        self.suffixes = stemming.get("suffixes", None)
        self.prefixes = stemming.get("prefixes", None)
        self.safe_words = stemming.get("safe_words", None)

        transformers = preprocessing_obj.get("transformers", dict())
        self.contractions = transformers.get("contractions", None)
        self.lemms = transformers.get("lemms", None)
        self.punctuations = transformers.get("punctuations", None)
        self.typos = transformers.get("typos", None)
        self.synonyms = transformers.get("synonyms", None)
        self.stop_sentences = transformers.get("stop_sentences", None)
        self.stop_words = transformers.get("stop_words", None)
        self.special_chars = transformers.get("special_chars", None)
