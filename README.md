﻿
# [![CodeWell](https://codewell.is/tools/images/codewell-logo.svg  "CodeWell")](https://codewell.is) AI Server  
  
This is a server that is used for **training the classifiers** and for **classifying the users' messages**.  

# Table of Contents
- [Setup with Docker](#setup-with-docker)
	- [For AI Server users](#for-ai-server-users)
	- [For AI Server development](#for-ai-server-development)
- [Setup without Docker](#setup-without-docker)
- [Deployments](#deployments)
  
# Setup with Docker  
  
1. Install **Docker** for your environment  
	- For [**MacOS**](https://docs.docker.com/docker-for-mac/install/)
	- For [**Linux**](https://docs.docker.com/engine/install/)
	- For [**Windows**](https://docs.docker.com/docker-for-windows/install/)
		- *or finally do something nice for yourself and start using Linux or MacOS*
2. Install [**docker-compose**](https://docs.docker.com/compose/install/#install-compose)
	> if you are on MacOS, probably you won't need to install this explicitly
4. Make sure your Docker engine is running
  
## For AI Server users  

You can use `docker-compose` to start and stop both the ai-server containers  and the database container together.  
  
This is the recommended setup for development purposes when you're **NOT** working on the **ai-server** itself, but need it as a service.  
  
### Starting up (first time)  
  
1. Navigate to the root of project  
2. Create an `.env` file based on the `env-example.txt` file.  
	- You can find the `env-example.txt` file in root of the project  
	- Modify the [environment variables](./docs/ENV_README.md) that have no value, or change any of the others  
	- Do not, I repeat, do not use `"" or ''` for the values.  
3. Before your first run you need to build the images locally  
	- `bash build-requirements-image.sh` *(we do this so subsequent builds, when the requirements are unchanged, are faster)*  
	- `docker-compose build`  
		- This will build the ai-server image and configure it with the default environment variables  
4. Then you can start them up  
	- `docker-compose up`  
		- This will start the database and the ai-server.

How things are set up:

- the address to the postgres database is set up, no need to change it  
- environment variables are set up when the containers are created  


### Starting up (second and other times)  
  
1. Navigate to the root of project  
2. `docker-compose up`  
  
### Changing Env Variables  
  
Environment variables are set when the containers are created. Running `docker-compose up` will not create the containers if they already exist  
  
To change the environment variables you will need to  
  
1. `docker-compose down`  
1. Edit the `.env` file  
1. `docker-compose up`  
  
### Accessing the AI server  
  
The AI server after being started with compose, runs in a container, and exposes the `33507 port` to localhost. The database is exposed at `port 5432`  
  
## For AI Server development  
  
Instead of installing Postgres as an application, you can start/stop the `postgres database` as a container.  

### First run  
  
We need to create a running container from an image of Postgres. Images provide the blueprint, containers are started and stopped.  
  
`docker run -e POSTGRES_PASSWORD=postgres -p 5432:5432 --name db -d postgres`  
  
What this does is tells docker to:  
  
- create a container from the `postgres` image  
- run the container in the background `-d`  
- the name of the container to be *db* `--name db`  
- map the `5432 port` of the container to be usable from *localhost:5432* `5432:5432`  
- set the environment variable `POSTGRES_PASSWORD` `-e POSTGRES_PASSWORD=postgres`. This will be the password for the `postgres` (super)user.  
  
Now you can connect to the database locally with  

- `postgresql://postgres:postgres@localhost:5432/postgres`   
	
	> The connection string has the following format `protocol://user:password@hostname:port/dbname`  
	> 	
	> Postgres by default creates the postgres database, the superuser named postgres and we specified the password for the superuser with `-e POSTGRES_PASSWORD=postgres`  
  
You can stop the database container with: `docker stop db`  
  
### Next run:  
  
Because the container is already created you can now just run:  
  
 `docker start db`  
  
### Removing container and deleting stale files  
  
Container must be stopped to be able to be removed. Then just execute the following commands:  

- `docker rm db`  

> Deletes the container named *db*  
> 
> **Note:** The image is still kept locally so creating a container next time will not redownload the image and it will be faster.  

- `docker volume prune`  

> When a container runs, it uses a volume for its disk space. When deleting containers, you can delete the volumes they used with this command. The command will delete all volumes that are not associated with any container.  
> 
> **Note:** If a container is stopped, but not removed, its volume (disk) will not be deleted.

  
## Building the AI Server docker image locally  
  
**Note:** If `requirements.txt` hasn't changed since the last build, just run the second command  
  
`bash build-requirements-image.sh`    
`docker build -t <image_name_and_tag> .`  
  
The image **name** and **tag** should reflect the name of the image and version.
By default, not specifying a tag will result to the implicit `latest` tag  
  
`docker build -t aiserver .`

```
$ docker image list  
REPOSITORY         TAG         IMAGE ID            CREATED             SIZE  
aiserver           latest      1fa3a85e914d        About an hour ago   615MB  
```

`docker build -t aiserver:0.1.1`

```
$ docker image list  
REPOSITORY         TAG         IMAGE ID            CREATED             SIZE  
aiserver           latest      1fa3a85e914d        About an hour ago   613MB  
aiserver           0.1.1       1fa3a85e914d        About a minute ago  613MB  
```

# Setup without Docker  
Why are you reading this part? (⊙_☉) Woudn't you use **Docker** like some manager type person?  
  
¯\\_(⊙_ʖ⊙)_/¯ ¯\\_(ツ)_/¯   
  
## install postgres  
1. click [here](https://postgresapp.com/) to download **Postgres.app**  
1. install **Postgres.app**  
1. when installed you should double click on the database with your username  
1. this should open a terminal.  
1. enter ```create database {your_pg_database_name};```  
  
## next steps  
1. Clone the [repository](https://bitbucket.org/codewell_unlimited/ai-server-boilerplate)  
2. Create a directory for your project  
3. Copy everything from the **ai-server-boilerplate** directory in your project's directory  
4. Open the project in your preferred IDE (Mine is PyCharm and all the following instructions are for PyCharm.)  
5. Open a terminal and navigate to your project's directory  
6. Execute `bash setup.sh`  
7. After the script is finished, open your IDE again  
8. Open PyCharm -> Preferences  (for MacOS)  
9. In the Settings/Preferences dialog `<⌃⌥S>`, select Project `<project name>` -> Project Interpreter. Click the configure icon (gear icon) and select Add.  
10. In the left-hand pane of the Add Python Interpreter dialog, select Virtualenv Environment.  
11. Select existing environment, click browse and navigate to `your-project-folder/virtualenvironment/bin/python` (we created the virtual environment with the `setup.sh` script)  
12. Add the [environment variables](./docs/ENV_README.md) from the `env-example.txt` (or see the next section)  
	
	> Run/Debug Configurations -> Environment variables  

1. Run `main.py`  
    
# Deployments  
  
## Deploy on Heroku  
  
You can check out how you can deploy your project on **Heroku** [here](./docs/HEROKU_README.md)  
  
  
![Sorry for the long post, here's a potato.](./pt.jpeg  "Sorry for the long post, here's a potato.")




