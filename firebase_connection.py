import os
from firebase_admin import db
from models.preprocessing_data import PreprocessingData

root_ref = os.environ.get("ENV")


class FirebaseConnection:
    def __init__(self, lang):
        self.classifying_data = None
        self.faqs_data = None
        self.states_data = None
        self.choices_data = None
        self.preprocessing_data = None

        self.lang = lang
        self.root = db.reference(root_ref)
        self.classifying_ref = "{}/classifying/{}".format(root_ref, lang)

        self.__get_classification_data()

    def __get_classification_data(self):
        self.classifying_data = self.root.child("classifying/{}".format(self.lang)).get()
        if not self.classifying_data:
            self.classifying_data = dict()

        preprocessing_obj = self.classifying_data.get("preprocessing", dict())

        train_obj = self.classifying_data.get("train_data", dict())
        faqs_train_obj = train_obj.get("faqs", dict())
        faqs_train_obj = faqs_train_obj.get("data", dict())
        scenarios_train_obj = train_obj.get("scenarios", dict())

        general_knowledge_data = faqs_train_obj.get("general_knowledge", None)
        specific_knowledge_data = faqs_train_obj.get("specific_knowledge", None)
        self.faqs_data = self.get_faqs_data(general_knowledge_data, specific_knowledge_data)

        self.states_data = scenarios_train_obj.get("states", None)
        self.choices_data = scenarios_train_obj.get("data", None)
        self.parent_choices_data = scenarios_train_obj.get("parent_data", None)

        self.preprocessing_data = PreprocessingData(preprocessing_obj)

    @staticmethod
    def get_faqs_data(general_knowledge_data, specific_knowledge_data):
        data = {}

        if general_knowledge_data:
            for groupKey in general_knowledge_data:
                question_data = general_knowledge_data.get(groupKey)
                for questionKey in question_data:
                    data[questionKey] = question_data.get(questionKey)

        if specific_knowledge_data:
            for groupKey in specific_knowledge_data:
                question_data = specific_knowledge_data.get(groupKey)
                for questionKey in question_data:
                    data[questionKey] = question_data.get(questionKey)

        return data

    def refresh(self):
        self.__get_classification_data()
