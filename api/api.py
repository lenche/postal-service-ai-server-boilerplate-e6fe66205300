import json
import logging
import time

from http.server import BaseHTTPRequestHandler
from typing import List

from api.request_handler import RequestHandler

from logger import Logger

log = Logger()


class Api(BaseHTTPRequestHandler):
    def __init__(self, request_handlers: List[RequestHandler]):
        self.handlers = dict()
        for handler in request_handlers:
            self.handlers[handler.endpoint()] = handler

    def __call__(self, *args):
        super().__init__(*args)

    def _set_headers(self, code):
        self.send_response(code)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
        self.send_header("Access-Control-Allow-Headers", "x-api-key,Content-Type")
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def do_OPTIONS(self):
        self._set_headers(200)
        self.send_response(200)

    def do_GET(self):
        self._set_headers(404)

    def do_POST(self):
        start_time = time.time()

        content_length = int(self.headers["Content-Length"])
        request_data = None

        if content_length > 0:
            request_data = json.loads(self.rfile.read(content_length))

        if self.path not in self.handlers:
            self._set_headers(404)
            return

        handler = self.handlers[self.path]
        try:
            log.info("request received on path [{}]...".format(self.path))
            log.info("request data [{}]".format(request_data))
            res = handler.handle_request(request_data)
            log.debug_end_time(start_time, "ended handling request on path [{}]".format(self.path))
            self._set_headers(200)
            self.wfile.write(json.dumps(res).encode("ascii"))
        except Exception as e:
            logging.exception("error handling request")
            self._set_headers(500)
