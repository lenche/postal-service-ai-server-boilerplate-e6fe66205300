import string
from typing import Any


class RequestHandler:
    def handle_request(self, request_data) -> Any:
        raise Exception('abstract method, must implement')

    def endpoint(self) -> string:
        raise Exception('abstract method, must implement')
