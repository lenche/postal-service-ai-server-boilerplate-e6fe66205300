import os
from http.server import HTTPServer
from socketserver import ForkingMixIn

from api.api import Api

from logger import Logger

log = Logger()


class ThreadedHTTPServer(ForkingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


class HttpServer:
    def __init__(self):
        self.host_name = ""
        self.port = int(os.environ.get('PORT', 33507))

    def run(self, api: Api):
        if self.host_name:
            log.info("server starts - host_name: [{}], port: [{}]".format(self.host_name, self.port))
        else:
            log.info("server starts - port: [{}]".format(self.port))

        server_address = ('0.0.0.0', self.port)
        httpd = ThreadedHTTPServer(server_address, api)

        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass

        httpd.server_close()

        if self.host_name:
            log.info("server stops - host_name: [{}], port: [{}]".format(self.host_name, self.port))
        else:
            log.info("server stops - port: [{}]".format(self.port))
