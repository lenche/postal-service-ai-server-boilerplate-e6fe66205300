import os
import time


class Logger:
    def __init__(self):
        if not os.environ.get("DEBUG_MODE"):
            self.debug_mode = False
        else:
            self.debug_mode = os.environ.get("DEBUG_MODE").lower() == "true"

    def info(self, log_message):
        self.__log("info", log_message)

    def info_end_time(self, start_time, log_message):
        self.__log_end_time("info", start_time, log_message)

    def debug(self, log_message):
        self.__log("debug", log_message)

    def debug_end_time(self, start_time, log_message):
        self.__log_end_time("debug", start_time, log_message)

    def error(self, log_message):
        self.__log("error", log_message)

    def error_end_time(self, start_time, log_message):
        self.__log_end_time("error", start_time, log_message)

    def __log(self, log_type, log_message):
        if log_type != "debug":
            print("* {} | {} | {}".format(log_type, time.asctime(), log_message))

        elif log_type == "debug" and \
                self.debug_mode:
            print("* {} | {} | {}".format(log_type, time.asctime(), log_message))

    def __log_end_time(self, log_type, start_time, log_message):
        end_time = time.time()
        duration = end_time - start_time

        log_message = "{} | ended in {}".format(log_message, duration)
        self.__log(log_type, log_message)
