﻿## Set Environment Variables
`ENV`=`development`    
`SHOULD_USE_CONVERSATION_TOPICS`=`true`    
`FAQ_THRESHOLD`=`0.8` (you can change this threshold later if needed)    
`SCENARIO_THRESHOLD`=`0.75` (you can change this threshold later if needed)    
`DECREASED_SCENARIO_THRESHOLD`=`0.6` (you can change this threshold later if needed)    
`CUSTOM_TRAINING_STATE_THRESHOLD`=`0.6` (you can change this threshold later if needed)    
`SUPPORTED_LANGUAGES`=`en,mk` (this is just an example, you should add your own supported languages)    
`DEFAULT_LANGUAGE`=`en` (this is just an example, you should add your own default language)    
`DEBUG_MODE`=`true` (change this variable to `false` when you don’t need the debug logs)    
`DATABASE_URL`=`postgresql://postgres:postgres@db:5432/postgres`    
`FIREBASE_SERVICE_ACCOUNT`=`{}` **you can find this on Firebase**    
`FIREBASE_API_KEY`=`string` **you can find this on Firebase** (looks like gibberish, something like: AIzaSyAtTw2h8IHlNQ0TyhfmuNaqlS9cT8F85Y4)     
`FIREBASE_DATABASE_URL`=`string` **you can find this on Firebase** (looks like a link, or something like this: `https://project-id.firebaseio.com`)    
`FIREBASE_AUTH_DOMAIN`=`string` **you can find this on Firebase** (looks something like this: `project-id.firebaseapp.com`)    
`FIREBASE_STORAGE_BUCKET`=`string` **you can find this on Firebase** (looks something like this: `project-id.appspot.com`)
