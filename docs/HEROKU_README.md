﻿
# Deploy your project on Heroku 
  
[**Heroku**](https://www.heroku.com) is a platform as a service (PaaS) that enables developers to build, run, and operate applications entirely in the cloud.  
  
## Track your project in git  
  
*Do this only if you didn't initialize a git repo previously*  
  
1. Open `terminal`  
2. Execute `cd path_to/your_project_folder`  
3. `git init`  
4. `git commit -m "First commit"`  
  
## Create a Heroku remote  
  
1. Open `terminal`  
2. [Install the Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) (if it isn't already installed)
3. Create а new Heroku app:  
	- open this [link](https://dashboard.heroku.com/apps) and click on `New` -> `Create new app`  
	- or execute this command from the terminal `heroku create project_name`  
4. Execute `heroku git:remote -a project_name`  
5. Execute `heroku stack:set container`  
6. `git push heroku master` or `git push heroku custom_branch:master` if your local branch is `custom_branch` and not `master`
  
## Finish setting up  
  
- Open the Heroku dashboard for your app.
	- You can find your app listed [here](https://dashboard.heroku.com/apps).  
- Open the `Resources` section and find `Add-ons`. Search for the `Heroku Postgres` add-on.
- Open the `Settings` section and find `Config Vars`.
	- The `DATABASE_URL` var is already set. Do not change it!
- Set the [environment variables](./ENV_README.md)
- Click `More` -> `Restart all dynos` (if they are not automatically restarted after editing the `config vars`)
