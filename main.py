# coding=utf-8
import threading
import os
from multiprocessing.shared_memory import SharedMemory

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

from typing import Callable
from sqlalchemy import create_engine
from multiprocessing import shared_memory
from classification.handlers.train_request_handler import TrainRequestHandler
from classification.handlers.preview_preprocessed_data_request_handler import PreviewPreprocessedDataRequestHandler
from classification.preprocessing.preprocessor import Preprocessor
from classification.trainer import Trainer
from models.classifiers_data import ClassifiersData

from firebase_connection import FirebaseConnection

from classification.preprocessing.profanity_checker import ProfanityChecker

from classification.handlers.classification_request_handler import ClassificationRequestHandler
from classification.handlers.classification_response_handler import ClassificationResponseHandler

from api.server import HttpServer
from api.api import Api
from models.repositories.classifiers import ClassifiersRepo
from models.repositories.dictionaries import DictionaryRepo

from firebase_config import FirebaseConfig


def create_daemon_thread(target: Callable[..., None]) -> threading.Thread:
    thread = threading.Thread(target=target)
    thread.daemon = True
    return thread


def generate_handlers(languages, database_conn) -> [SharedMemory]:
    generated_handlers = []
    smem = []
    for lang in languages:
        # components
        classifiers_repo = ClassifiersRepo(database_conn, lang)
        dictionary_repo = DictionaryRepo(database_conn, lang)
        firebase_connection = FirebaseConnection(lang)

        preprocessor = Preprocessor(firebase_connection)
        profanity_checker = ProfanityChecker(firebase_connection.preprocessing_data.profanities)

        classification_response_handler = ClassificationResponseHandler(lang)

        has_any_classifier = classifiers_repo.has_any_classifier()
        classifiers_container = ClassifiersData(dict())

        train_data_version_sm = shared_memory.SharedMemory(create=True, size=8)
        smem.append(train_data_version_sm)
        trainer: Trainer

        if has_any_classifier:
            # get existing classifiers data

            all_classifiers_dict = classifiers_repo.get_all_classifiers()
            dictionary = dictionary_repo.get_dictionary()
            classifiers_container.set_dictionary(dictionary)
            classifiers_container.set_train_data(all_classifiers_dict)
            trainer = Trainer(classifiers_repo, dictionary_repo, firebase_connection, classifiers_container,
                              preprocessor, lang, train_data_version_sm)
        else:
            # generate new classifiers data
            trainer = Trainer(classifiers_repo, dictionary_repo, firebase_connection, classifiers_container,
                              preprocessor, lang, train_data_version_sm)
            daemon_thread = create_daemon_thread(lambda: trainer.start_training())
            daemon_thread.start()

        classification_request_handler = ClassificationRequestHandler(
            classification_response_handler,
            firebase_connection,
            trainer,
            profanity_checker,
            preprocessor,
            lang,
            train_data_version_sm)

        generated_handlers.append(classification_request_handler)

        train_request_handler = TrainRequestHandler(trainer, lang)
        generated_handlers.append(train_request_handler)

        preview_data_request_handler = PreviewPreprocessedDataRequestHandler(trainer, lang)
        generated_handlers.append(preview_data_request_handler)

    return generated_handlers, smem


if __name__ == '__main__':
    firebase = FirebaseConfig()

    pg_database_url = os.environ['DATABASE_URL']
    database_engine = create_engine(pg_database_url)
    database_connection = database_engine.connect()
    server = HttpServer()

    supported_languages = os.environ.get("SUPPORTED_LANGUAGES")
    default_language = os.environ.get("DEFAULT_LANGUAGE")

    if not supported_languages:
        supported_languages = [default_language]
    else:
        supported_languages = supported_languages.split(",")

    handlers, sm = generate_handlers(supported_languages, database_connection)

    api = Api(handlers)
    server.run(api)
    for s in sm:
        s.close()
